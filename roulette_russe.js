var readlineSync = require('readline-sync');

const balles = readlineSync.questionInt("Nombre de balles dans la chambre : ", {limit: '$<1-8>'});
const joueurs = readlineSync.questionInt("1 ou 2 joueurs : ", {limit: '$<1-2>'});

if(joueurs == 1) {
  gameOnePlayer();
}
else {
  gameTwoPlayers();
}
function gameOnePlayer() {
  var tours = 0;
  var fin = false;
  while(!fin) {
    console.log(`Tours survécus : ${tours}`);
    if(readlineSync.keyInYN("Voulez-vous continuer?")) {
      //Random entre 0 et 7 pour bien être à n / 8
      if(Math.floor(Math.random() * 7) > balles - 1) {
        console.log("Vous avez survécu...");
      }
      else {
        console.log("MORT");
        fin = true;
      }
      tours++;
    }
    else {
      console.log(`Vous avez survécu ${tours} avant de vous défiler`);
      fin = true;
    }
  }
}

async function gameTwoPlayers() {
  var tours = 0;
  var first_player = true;
  var fin = false;
  while(!fin) {

    first_player ? console.log("Le premier joueur prend l'arme...") : console.log("Le deuxième joueur prend l'arme...");
    await sleep(1000);

    //Random entre 0 et 7 pour bien être à n / 8
    if(Math.floor(Math.random() * 7) > balles - 1) {
      console.log("Vous avez survécu...");
      await sleep(1000);
      if(!first_player) {
        tours++;
        console.log(`Tours passés : ${tours}`);
        await sleep(1000);
      };
      first_player = !first_player;
    }
    else {
      console.log("MORT");
      if(first_player) {
        console.log(`Le premier joueur est mort... Il aura survécu ${tours} tours...`);
      }
      else {
        console.log(`Le deuxième joueur est mort... Il aura survécu ${tours} tours...`);
      }
      fin = true;
    }
  }
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
