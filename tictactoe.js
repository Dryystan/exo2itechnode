var readlineSync = require('readline-sync');

var grille = [[' - ', ' - ', ' - '], [' - ', ' - ', ' - '], [' - ', ' - ', ' - ']];
var gagne = false;

game();

function game() {
  let gagnant = -1;
  let i = 0
  let player_one = true;
  while( i < 9 && !gagne) {
    afficherGrille();
    player_one ? console.log("Tour du joueur 1 (X)") : console.log("Tour du joueur 2 (O)");
    let play_valide = false;
    let colonne;
    let ligne;
    while(!play_valide) {
      colonne = readlineSync.questionInt("Colonne : ", {limit: '$<1-3>'});
      ligne = readlineSync.questionInt("Ligne : ", {limit: '$<1-3>'});
      if(grille[ligne-1][colonne-1] !== ' - ') {
        console.log("Veuillez choisir une case vide!");
      }
      else {
        play_valide = true;
      }
    }
    play(player_one, colonne, ligne);
    gagnant = verif_victoire();
    player_one = !player_one;
    i++;
  }
  afficherGrille();
  gagne ? console.log(`Le joueur ${gagnant} a gagné!`) : console.log("C'est une égalité!");
}

function verif_victoire() {
  //Pour tester première ligne, dernière colonne et diagonale partant d'en haut à droite
  let signe_courant = grille[0][2];
  if(signe_courant !== ' - ' && ((signe_courant === grille[0][1] && signe_courant === grille[0][0])
      || (signe_courant === grille[1][2] && signe_courant === grille[2][2])
      || (signe_courant === grille [1][1] && signe_courant === grille[2][0])))
  {
    gagne = true;
    return signe_courant === " X " ? 1 : 2;
  }

  // Verification croix partant du centre et deuxième diagonale
  signe_courant = grille[1][1];
  if(signe_courant !== ' - ' && ((signe_courant === grille[0][1] && signe_courant === grille[2][1])
      || (signe_courant === grille[1][0] && signe_courant === grille[1][2])
      || (signe_courant === grille [0][0] && signe_courant === grille[2][2])))
  {
    gagne = true;
    return signe_courant === " X " ? 1 : 2;
  }

  // Verification premiere colonne et dernière ligne
  signe_courant = grille[2][0];
  if(signe_courant !== ' - ' && ((signe_courant === grille[2][1] && signe_courant === grille[2][2])
      || (signe_courant === grille[1][0] && signe_courant === grille[0][0])))
  {
    gagne = true;
    return signe_courant === " X " ? 1 : 2;
  }
}

function play(player_one, colonne, ligne) {
  let signe = "X";
  if(!player_one) {
    signe = "O";
  }
  grille[ligne-1][colonne-1] = ` ${signe} `;
}

function afficherGrille() {
  console.log("   1   2   3");
  console.log("1 " + grille[0]);
  console.log("2 " + grille[1]);
  console.log("3 " + grille[2]);
}
