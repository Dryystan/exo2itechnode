var readlineSync = require('readline-sync');

var n = readlineSync.questionInt("Nombre de lignes d'étoiles: ");
for(let i = 1; i <= n; i++) {
  console.log("*".repeat(i));
}
