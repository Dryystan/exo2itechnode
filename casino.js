var readlineSync = require('readline-sync');
// Objet contenant toutes les possibilités
// Cases représente les cases jouables, cases_valides représente le nombre valides à partir de la case proposé
// Ex: Transversale, case jouable 4, case valide (à partir de 4): +0, +1, +2, donc 4, 5, 6
const possibilites = {
  1: {
    nom: "PLEIN",
    detail: "La mise est placée sur un seul numéro. Le joueur gagne 35 fois sa mise.",
    gain: 35,
    cases: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36],
    cases_valides: [0]
  },
  2: {
    nom: "TRANSVERSALE",
    detail: "La mise est placée sur la ligne extérieur d'une rangée horizontale soit 3 numéros. Le joueur gagne 11 fois sa mise.",
    gain: 11,
    cases: [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31, 34],
    cases_valides: [0, 1, 2]
  },
  3: {
    nom: "CHEVAL",
    detail: "La mise est placée à cheval sur 2 numéros. Le joueur gagne 17 fois sa mise.",
    gain: 17,
    cases: [1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32, 34, 35],
    cases_valides: [0, 1]
  },
  4: {
    nom: "SIXAIN",
    detail: "La mise est placée sur la ligne extérieure à l'intersection de 2 rangées horizontales, soit 6 numéros. Le joueur gagne 5 fois sa mise.",
    gain: 5,
    cases: [1, 4, 7, 10, 13, 16, 19, 22, 25, 28, 31],
    cases_valides: [0, 1, 2, 3, 4, 5]
  },
  5: {
    nom: "CARRE",
    detail: "La mise est placée à l'intersection de 4 numéros. Le joueur gagne 8 fois sa mise.",
    gain: 8,
    cases: [1, 2, 4, 5, 7, 8, 10, 11, 13, 14, 16, 17, 19, 20, 22, 23, 25, 26, 28, 29, 31, 32],
    cases_valides: [0, 1, 3, 4]
  },
  6: {
    nom: "DOUZAINE",
    detail: "La mise est placée dur une des 3 zones suivantes : soit 12 numéros joués, 12 P (12 premiers numéros), 12 M (12 numéros du milieu), 12 D (12 derniers numéros). Le joueur gagne 2 fois sa mise. Si le 0 sort, les douzaines sont perdantes.",
    gain: 2,
    cases: [1, 13, 25],
    cases_valides: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
  },
  7: {
    nom: "DOUZAINE A CHEVAL",
    detail: "La mise est placée à l'intersection de 2 douzaines, soit 24 numéros joués. Le joueur gagne une demi-fois sa mise.",
    gain: 0.5,
    cases: [1, 13],
    cases_valides: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
  },
  8: {
    nom: "COLONNE",
    detail: "La mise est placée au bas d'une des 3 colonnes verticales, soit 12 numéros. Le joueur gagne 2 fois sa mise. Si le 0 sort, les colonnes sont perdantes.",
    gain: 2,
    cases: [34, 35, 36],
    cases_valides: [0, -3, -6, -9, -12, -15, -18, -21, -24, -27, -30, -33]
  },
  9: {
    nom: "COLONNE A CHEVAL",
    detail: "La mise est placée à l'intersection de 2 colonnes verticales, soit 24 numéros. Le joueur gagne une demi-fois la mise.",
    gain: 0.5,
    cases: [34, 35],
    cases_valides: [0, 1, -3, -2, -6, -5, -9, -8, -12, -11, -15, -14, -18, -17, -21, -20, -24, -23, -27, -26, -30, -29, -33, -32]
  }
};
var jetons = 0;
const COUPS_POSSIBLES = Object.keys(possibilites);

init();
game();

function init() {
  jetons = readlineSync.questionInt('Nombre de jetons : ');
}

async function game() {
  var done = false;
  while(!done) {
    COUPS_POSSIBLES.forEach(key => {
      console.log(`${key} : ${possibilites[key].nom} / ${possibilites[key].detail}\nGain: ${possibilites[key].gain}`);
    })
    let coup_joue = readlineSync.question('Coup à jouer : ', {limit: COUPS_POSSIBLES});
    coup_joue = parseInt(coup_joue);

    console.log(`Cases possibles: ${possibilites[coup_joue].cases}`);
    let case_joue = readlineSync.question('Case : ', {limit: possibilites[coup_joue].cases});
    case_joue = parseInt(case_joue);

    console.log(`Jetons restants : ${jetons}`);
    let limite = `$<1-${jetons}>`;
    let mise_joue = readlineSync.questionInt('Mise : ');
    while(mise_joue < 0 || mise_joue > jetons) {
      console.log('Veuillez entrer un nombre valide de jetons');
      console.log(`Jetons restants : ${jetons}`);
      mise_joue = readlineSync.questionInt('Mise : ');
    }

    let cases_valides_coup = [];
    possibilites[coup_joue].cases_valides.forEach(v => cases_valides_coup.push(v+case_joue));
    console.log(`Cases gagnantes: ${cases_valides_coup.sort(function(a, b) {return a - b;})}`);
    await sleep(1000);
    console.log('Faites vos jeux!');
    await sleep(1000);
    console.log('La balle est lancée...');
    await sleep(1000);
    process.stdout.write('.');
    await sleep(1000);
    process.stdout.write('.');
    await sleep(1000);
    process.stdout.write('.');
    await sleep(1000);
    process.stdout.write('.\n');
    await sleep(1000);
    let resultat = Math.floor(Math.random() * 36);
    console.log(`La balle s'est arrêtée sur ${resultat}!`)
    await sleep(1000);

    if(cases_valides_coup.includes(resultat)) {
      let gain = mise_joue * possibilites[coup_joue].gain;
      if(gain < 1) gain = 1;
      console.log(`Vous avez gagné ${gain} jetons!`)
      jetons += gain
    }
    else {
      console.log(`Vous avez perdu votre mise de ${mise_joue} jetons...`);
      jetons -= mise_joue;
    }

    console.log(`Il vous reste ${jetons} jetons.`);
    if(readlineSync.keyInYN('Voulez-vous continuer?')) {
      if(jetons === 0) {
        console.log(`Vous n'avez plus de jetons... C'est l'heure de commencer une nouvelle partie!`);
        init()
      }
      else {
        console.log('C\'est reparti!');
      }
    }
    else {
      done = true;
    }
  }
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
