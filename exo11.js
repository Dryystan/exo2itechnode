var readlineSync = require('readline-sync');

var montant = readlineSync.questionFloat("Argent: ").toFixed(2);
var monnaie = [500, 200, 100, 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05, 0.02, 0.01];

var montant_restant = montant;
monnaie.forEach(value => {
  if(montant_restant / value >= 1)
  {
    if(value >= 5) {
      console.log(`Billet ${value}€ : ${parseInt(montant_restant / value)}`);
    }
    else {
      console.log(`Pièce de ${value}€ : ${parseInt(montant_restant / value)}`);
    }
  }
  montant_restant = (montant_restant % value).toFixed(2);
});
