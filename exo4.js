var int_array = [12, 54, 32];
console.log(Math.max(...int_array));

var bigger = int_array[0];
for(let num of int_array) {
  if(num > bigger) {
    bigger = num;
  }
}
console.log(bigger);
