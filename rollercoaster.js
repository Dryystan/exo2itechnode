var readlineSync = require('readline-sync');

//Initialisation
const places = readlineSync.questionInt("Nombre de places : ");
const tours = readlineSync.questionInt("Nombre de tours : ");
const nb_groupes = readlineSync.questionInt("Nombre de groupes : ");
let i = 0;
const groupes = [];
while (++i <= nb_groupes) {
  groupes.push(readlineSync.questionInt(`Taille groupe ${i} : `));
}

var gains = 0;
for(let i = 0; i < tours; i++) {
  let places_utilisees = 0;
  let groupes_tempo = [];
  while(groupes.length > 0 && places_utilisees + groupes[0] <= places) {
    places_utilisees += groupes[0];
    groupes_tempo.push(groupes.shift());
  }
  groupes.push(...groupes_tempo);
  gains += places_utilisees;
}

console.log(`Gains totaux : ${gains}€.`);
