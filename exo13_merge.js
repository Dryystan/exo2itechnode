var int_array = [12, 25, 5, 7, 6, -5];

console.log(merge_sort(int_array));

function merge_sort(m) {
  if(m.length <= 1) {
    return m;
  }

  var left = [];
  var right = [];
  for(let index in m) {
    if(index < m.length / 2) {
      left.push(m[index]);
    }
    else {
      right.push(m[index]);
    }
  }

  left = merge_sort(left);
  right = merge_sort(right);

  return merge(left, right);
}

function merge(left, right) {
  var result = [];

  while(left.length > 0 && right.length > 0) {
    if(left[0] <= right[0]) {
      result.push(left.shift());
    }
    else {
      result.push(right.shift());
    }
  }
  while(left.length > 0) {
    result.push(left.shift());
  }

  while(right.length > 0) {
    result.push(right.shift());
  }

  return result;
}
