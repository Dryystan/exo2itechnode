var readlineSync = require('readline-sync');

var n = readlineSync.questionInt("Nombre limite : ");
for(let i = 1; i <= n; i++) {
  if(i % 3 == 0) {
    process.stdout.write("FIZZ");
  }
  if(i % 5 == 0) {
    process.stdout.write("BUZZ");
  }
  if(i % 3 != 0 && i % 5 != 0) {
    process.stdout.write(i.toString());
  }
  process.stdout.write("\n");
}
