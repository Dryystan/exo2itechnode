var notes_array = [0, 0, 2, 3, 5, 6, 7, 9, 10, 12, 12, 13, 15, 17, 18, 19, 20];
notes_array.forEach(note => {
  if(note <= 4) {
    console.log(`${note} : Catastrophique, il faut tout revoir`);
  }
  else if(note <= 10) {
    console.log(`${note} : Insuffisant`);
  }
  else if(note <= 14) {
    console.log(`${note} : Peut mieux faire`);
  }
  else if(note <= 17) {
    console.log(`${note} : Bien`);
  }
  else {
    console.log(`${note} : Excellent, bon travail`);
  }
})
