var temperatures = [12, 25, 5, 7, 6, -5, -4];
var closest_to_zero = temperatures[0];
for(let temperature of temperatures) {
  if(Math.abs(temperature) < Math.abs(closest_to_zero)) {
    closest_to_zero = temperature;
  }
}
console.log(`La température la plus proche de 0°C est ${closest_to_zero}°C.`)
