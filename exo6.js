var readlineSync = require('readline-sync');

var x = readlineSync.questionInt("Nombre 1: ");
var operateur = readlineSync.question("Opérateur: ");
var y = readlineSync.questionInt("Nombre 2: ");
let res;
switch(operateur) {
  case "+":
    res = x + y;
    break;
  case "-":
    res = x - y;
    break;
  case "*":
    res = x * y;
    break;
  case "/":
    res = x / y;
    break;
}
console.log(`${x} ${operateur} ${y} = ${res}`);
