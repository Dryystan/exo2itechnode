var readlineSync = require('readline-sync');

const key = 1;
var message = readlineSync.question("Message à chiffrer : ");
var result = "";
for(let c of message) {
  if(isLetter(c)) {
    result += incrementLetter(c, key);
  }
  else {
    result += c;
  }
}
console.log(result);

function incrementLetter(c, increment) {
  //97 => a, 122 => z
  if(c.charCodeAt(0) >= 97) {
    var ccode = c.charCodeAt(0) - 97;
    ccode = (ccode + increment) % 26;
    return String.fromCharCode(ccode + 97);
  }
  else {
    var ccode = c.charCodeAt(0) - 65;
    ccode = (ccode + increment) % 26;
    return String.fromCharCode(ccode + 65);
  }
}

function isLetter(str) {
  return str.length === 1 && (str.match(/[a-z]/i) || str.match(/[A-Z]/i));
}
