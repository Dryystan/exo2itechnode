var readlineSync = require('readline-sync');
const fs = require("fs");

const word = getRandomLine("liste_francais.txt");
var vies = 10;
var gagne = false;
var hidden_word = "";
let i = word.length;
while (i--) {
  hidden_word += "_";
}

game();

function game() {
  console.log(hidden_word);

  while(vies > 0 && !gagne) {
    var character = readlineSync.keyIn("Entrez une lettre : ", {limit: '$<a-z>'});
    if(word.includes(character)) {
      for(let [index, c] of Array.from(word).entries()) {
        if(c == character) {
          hidden_word = replaceAt(hidden_word, index, c);
        }
      }
    }
    else {
      console.log("Loupé");
      vies--;
    }
    if(!hidden_word.includes("_")) {
      gagne = true;
    }

    console.log(hidden_word);
  }

  finPartie(gagne);
}

function finPartie(gagne) {
  if(gagne) {
    console.log("Bravo, vous avez gagné!");
  }
  else {
    console.log(`Perdu... Le mot était ${word}`);
  }
}

function getRandomLine(filename){
  var data = fs.readFileSync(filename, {encoding:'utf8', flag:'r'});
  var lines = data.split('\r\n');
  var line = lines[Math.floor(Math.random()*lines.length)];
  return line;
}

function replaceAt(word, index, replacement) {
    return word.substr(0, index) + replacement + word.substr(index + replacement.length);
}
