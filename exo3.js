var readlineSync = require('readline-sync');

let cout_fabrication = readlineSync.questionInt("Cout de fabrication: ");
let prix_vente = readlineSync.questionInt("Prix de vente: ");
let diff = Math.abs(cout_fabrication - prix_vente);
console.log(cout_fabrication < prix_vente ? `Profit de ${diff}€` : `Perte de ${diff}€`);
