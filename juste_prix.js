var readlineSync = require('readline-sync');

const nombre_cache = Math.floor(Math.random() * readlineSync.questionInt("Quel est le nombre maximal? "));
var won = false;

console.log("Vous avez 30 secondes!");
const s = new Date().getTime();
while (won == false) {
  if (new Date().getTime() - s >= 30000) {
    console.log(`Le temps de 30 secondes est écoulé! Le nombre était ${nombre_cache}.`)
    break;
  }
  var nombre_user = readlineSync.questionInt("Proposition : ");
  if(nombre_user > nombre_cache) {
    console.log("Plus bas!");
  }
  else if(nombre_user < nombre_cache) {
    console.log("Plus haut!");
  }
  else {
    console.log(`Trouvé en ${(new Date().getTime() - s) / 1000} secondes!`);
    won = true;
  }
}
